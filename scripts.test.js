const multiply = require('./scripts');

test('2 * 2 * 2 should output 8', () =>{
    const operation = multiply(2,2,2);
    expect(operation).toBe(8);
    const operation2 = multiply(3,3,3);
    expect(operation2).toBe(27);
});

test('should output a parameter is null', () =>{
    const operation = multiply(null,2,2);
    expect(operation).toBe(0);
});

test('should output a parameter is a string', () =>{
    const operation = multiply('null',2,2);
    expect(operation).toBe(NaN);
});

test('should output missing 1 parameter', () =>{
    const operation = multiply(2,2);
    expect(operation).toBe(NaN);
});

test('should output missing parameters', () =>{
    const operation = multiply();
    expect(operation).toBe(NaN);
});